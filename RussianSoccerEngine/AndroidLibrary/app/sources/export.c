#include "paper-football.h"
#include "export.h"

struct ai* create_ai_storage()
{
    struct ai* storage = malloc(sizeof(struct ai));
    return storage;
}

void destroy_ai_storage(struct ai* storage)
{
    free(storage);
}

int state_get_ball(const struct state * const me)
{
    return me->ball;
}

int state_get_active(const struct state * const me)
{
    return me->active;
}
