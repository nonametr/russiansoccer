﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ScreenShotMaker : MonoBehaviour
{
    public string fileName = "ru_soccer";
    public int startImgId = 1;
    public void makeScreenShot()
    {
        ScreenCapture.CaptureScreenshot(fileName + "_" + startImgId++ + ".png");
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(ScreenShotMaker))]
public class ScreenShotMakerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ScreenShotMaker maker = (ScreenShotMaker)target;
        if (GUILayout.Button("CaptureScreenshot"))
        {
            maker.makeScreenShot();
        }
    }
}
#endif