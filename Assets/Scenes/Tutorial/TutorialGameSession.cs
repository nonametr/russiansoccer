﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Doozy.Engine;

public class TutorialGameSession : MonoBehaviour
{
    public int tutorialProgress = 0;
    public GameSession game;
    public Button nextButton;
    public GameObject nextTurnObject;

    public SelfPlayer[] player = new SelfPlayer[] { null, null, null };

    private void Awake()
    {
        GameSession.mode = GameMode.TUTORIAL;
    }
    void Start()
    {
        game = GetComponent<GameSession>();

        player[1] = new SelfPlayer(1, game);
        player[2] = new SelfPlayer(2, game);

        nextButton.gameObject.SetActive(true);
    }
    public void onTutorialBegin(int partId)
    {
        nextButton.gameObject.SetActive(partId <= tutorialProgress);
    }
    public void onTutorialPass(int partId)
    {
        tutorialProgress = (partId > tutorialProgress) ? partId : tutorialProgress;

        GameEventMessage.SendEvent("TriggerNext");
    }
    public Vector3 toV3(Vector2Int pos)
    {
        return new Vector3(pos.x, pos.y, 0);
    }
    private IEnumerator waitForSteps(Step[] steps)
    {
        Step lastStep = Step.QSTEPS;
        Action<Step> handler = (Step tryStep) => { lastStep = tryStep; };
        PossibleTurnPoint.onPlayerStepTry += handler;

        while (!steps.Contains(lastStep))
            yield return null;

        PossibleTurnPoint.onPlayerStepTry -= handler;
    }
    private IEnumerator waitForStep(Step step)
    {
        Step lastStep = Step.QSTEPS;
        Action<Step> handler = (Step tryStep) => { lastStep = tryStep; };
        PossibleTurnPoint.onPlayerStepTry += handler;

        while (lastStep != step)
            yield return null;

        PossibleTurnPoint.onPlayerStepTry -= handler;
    }
    public bool isBallReady()
    {
        if (Ball.inst.stepPoints.Count == 0)
            return false;

        foreach (var kv in Ball.inst.stepPoints)
        {
            if (kv.Value.sprite == null)
                return false;
        }
        return true;
    }
    public void blockAllTurns()
    {
        foreach (var kv in Ball.inst.stepPoints)
        {
            kv.Value.sprite.color = Ball.inst.impossibleTurn;
            kv.Value.sprite.enabled = false;
        }
    }
    public void allowSteps(Step[] steps, Color color)
    {
        if (!isBallReady())
            return;

        foreach (var kv in Ball.inst.stepPoints)
        {
            kv.Value.sprite.color = Ball.inst.impossibleTurn;
            kv.Value.sprite.enabled = false;
        }

        foreach (Step step in steps)
        {
            Ball.inst.stepPoints[step].sprite.enabled = true;
            Ball.inst.stepPoints[step].sprite.color = color;
        }
    }
    public IEnumerator waitExpectedPenalty(Step step)
    {
        allowSteps(new Step[] { step }, Ball.inst.penaltyTurn);

        Vector2Int targetPos = PointRemap.stepToPos(Ball.pos, step);

        nextTurnObject.SetActive(true);
        nextTurnObject.transform.position = toV3(targetPos);
        yield return waitForStep(step);

        for (int i = 0; i < 5; ++i)
        {
            player[1].addTryLine(Ball.pos, targetPos, step);
            Ball.pos = targetPos;
            targetPos = PointRemap.stepToPos(targetPos, step);
        }

        nextTurnObject.SetActive(false);
    }
    public IEnumerator waitUndoStep(Step step)
    {
        allowSteps(new Step[] { step }, Ball.inst.revertTurn);

        Vector2Int targetPos = PointRemap.stepToPos(Ball.pos, step);
        nextTurnObject.SetActive(true);
        nextTurnObject.transform.position = toV3(targetPos);
        yield return waitForStep(step);

        player[1].delTryLine(Ball.pos, targetPos, step);
        Ball.pos = targetPos;
        nextTurnObject.SetActive(false);
    }
    public IEnumerator waitExpectedPosition(Step step)
    {
        allowSteps(new Step[] { step }, Ball.inst.possibleTurn);

        Vector2Int targetPos = PointRemap.stepToPos(Ball.pos, step);

        nextTurnObject.SetActive(true);
        nextTurnObject.transform.position = toV3(targetPos);
        yield return waitForStep(step);

        player[1].addTryLine(Ball.pos, targetPos, step);
        Ball.pos = targetPos;
        nextTurnObject.SetActive(false);
    }
    public IEnumerator waitForCommitTurn()
    {        
        blockAllTurns();

        nextTurnObject.SetActive(true);
        nextTurnObject.transform.position = toV3(Ball.pos);

        bool commitDone = false;
        Action handler = () => { commitDone = true; };

        CurrentPoint.onPlayerTurnCommit += handler;

        while (commitDone == false)
            yield return null;


        commitTurn();
        CurrentPoint.onPlayerTurnCommit -= handler;
        nextTurnObject.SetActive(false);
    }
    public IEnumerator simulateOponentTurn(List<Step> steps, float stepDelay = 0.25f)
    {
        foreach (Step step in steps)
        {
            Vector2Int newPos = PointRemap.stepToPos(Ball.pos, step);
            LineRenderer lr;
            if(game.board.lines.TryGetValue(new Line(Ball.pos, newPos), out lr))
            {
                lr.gameObject.SetActive(true);
                lr.material = game.board.normalLineMaterial;
                lr.material.color = Color.blue;
                lr.startColor = Color.blue;
                lr.endColor = Color.blue;
            }
            
            Ball.pos = newPos;
            yield return new WaitForSeconds(stepDelay);
        }
    }
    private void commitTurn()
    {
        DashedLineHighlight[] lines = FindObjectsOfType<DashedLineHighlight>();
        foreach (DashedLineHighlight line in lines)
        {
            LineRenderer lr = line.GetComponent<LineRenderer>();
            lr.material = game.playerMaterials[1];

            Destroy(line);
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(TutorialGameSession))]
public class TutorialGameSessionEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        TutorialGameSession myScript = (TutorialGameSession)target;
        if (GUILayout.Button("Serialzie board"))
        {
            string data = myScript.game.board.serializeBoard();
            Debug.Log(data);
        }
    }
}
#endif