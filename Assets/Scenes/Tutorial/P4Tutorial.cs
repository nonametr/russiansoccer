﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine;

public class P4Tutorial : MonoBehaviour
{
    public TutorialGameSession tutorial;
    public string boardState;

    void OnEnable()
    {
        tutorial.game.board.deserializeBoard(boardState);

        Ball.pos = new Vector2Int(4, 21);

        StartCoroutine(tutorialCoroutine());
    }
    private void OnDisable()
    {
        StopCoroutine(tutorialCoroutine());
    }
    private IEnumerator tutorialCoroutine()
    {
        tutorial.onTutorialBegin(4);

        yield return tutorial.waitExpectedPosition(Step.EAST);
        yield return tutorial.waitExpectedPosition(Step.EAST);
        yield return tutorial.waitUndoStep(Step.WEST);
        yield return tutorial.waitUndoStep(Step.WEST);
        yield return tutorial.waitExpectedPosition(Step.SOUTH_EAST);
        yield return tutorial.waitExpectedPosition(Step.EAST);
        yield return tutorial.waitExpectedPosition(Step.NORTH_EAST);
        yield return tutorial.waitForCommitTurn();

        yield return new WaitForSeconds(0.25f);
        yield return tutorial.simulateOponentTurn(new List<Step>() { Step.EAST, Step.EAST, Step.SOUTH_EAST });

        tutorial.onTutorialPass(4);
    }
}
