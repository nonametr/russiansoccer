﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine;

public class P3Tutorial : MonoBehaviour
{
    public TutorialGameSession tutorial;
    public string boardState;

    void OnEnable()
    {
        tutorial.game.board.deserializeBoard(boardState);

        Ball.pos = new Vector2Int(8, 20);

        StartCoroutine(tutorialCoroutine());
    }
    private void OnDisable()
    {
        StopCoroutine(tutorialCoroutine());
    }
    private IEnumerator tutorialCoroutine()
    {
        tutorial.onTutorialBegin(3);

        yield return tutorial.waitExpectedPosition(Step.SOUTH_EAST);
        yield return tutorial.waitExpectedPosition(Step.SOUTH_WEST);
        yield return tutorial.waitExpectedPosition(Step.NORTH);
        yield return tutorial.waitExpectedPenalty(Step.WEST);
        yield return tutorial.waitForCommitTurn();

        yield return new WaitForSeconds(0.25f);
        yield return tutorial.simulateOponentTurn(new List<Step>() { Step.NORTH, Step.NORTH, Step.EAST });

        tutorial.onTutorialPass(3);
    }
}
