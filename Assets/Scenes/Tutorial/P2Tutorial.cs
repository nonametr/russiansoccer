﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine;
public class P2Tutorial : MonoBehaviour
{
    public TutorialGameSession tutorial;
    public string boardState;

    void OnEnable()
    {
        tutorial.game.board.deserializeBoard(boardState);

        Ball.pos = new Vector2Int(8, 16);

        StartCoroutine(tutorialCoroutine());
    }
    private void OnDisable()
    {
        StopCoroutine(tutorialCoroutine());
    }

    private IEnumerator tutorialCoroutine()
    {
        tutorial.onTutorialBegin(2);

        yield return tutorial.waitExpectedPosition(Step.NORTH);
        yield return tutorial.waitExpectedPosition(Step.NORTH_WEST);
        yield return tutorial.waitForCommitTurn();

        yield return new WaitForSeconds(0.25f);
        yield return tutorial.simulateOponentTurn(new List<Step>() { Step.NORTH, Step.NORTH, Step.EAST });

        tutorial.onTutorialPass(2);
    }
}
