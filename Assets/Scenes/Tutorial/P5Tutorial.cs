﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P5Tutorial : MonoBehaviour
{
    public TutorialGameSession tutorial;
    public string boardState;

    void OnEnable()
    {
        tutorial.game.board.deserializeBoard(boardState);

        Ball.pos = new Vector2Int(10, 20);

        StartCoroutine(tutorialCoroutine());
    }
    private void OnDisable()
    {
        StopCoroutine(tutorialCoroutine());
    }
    private IEnumerator tutorialCoroutine()
    {
        tutorial.onTutorialBegin(5);

        yield return tutorial.waitExpectedPosition(Step.NORTH);
        yield return tutorial.waitExpectedPosition(Step.NORTH_WEST);
        yield return tutorial.waitExpectedPosition(Step.NORTH_WEST);

        tutorial.onTutorialPass(5);
    }
}
