﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishTutorial : MonoBehaviour
{
    public TutorialGameSession tutorial;
    public string boardState;

    void OnEnable()
    {
        //tutorial.game.board.deserializeBoard(boardState);

        StartCoroutine(tutorialCoroutine());
    }
    private IEnumerator tutorialCoroutine()
    {
        yield return new WaitForSeconds(1);
        tutorial.nextButton.gameObject.SetActive(true);
    }
}
