﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine;
using System;

public class P1Tutorial : MonoBehaviour
{
    public TutorialGameSession tutorial;

    void OnEnable()
    {
        tutorial.game.board.resetBoard();

        Ball.pos = new Vector2Int(7, 15);

        StartCoroutine(tutorialCoroutine());
    }
    private void OnDisable()
    {
        StopCoroutine(tutorialCoroutine());
    }
    private IEnumerator tutorialCoroutine()
    {
        tutorial.onTutorialBegin(1);

        yield return tutorial.waitExpectedPosition(Step.NORTH_EAST);

        tutorial.onTutorialPass(1);
    }
}
