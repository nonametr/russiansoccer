﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Doozy.Engine;

public enum GameMode { TUTORIAL = 0, LOCAL_PVP, PVAI, NETWORK_PVP }
public class StartSceneController : MonoBehaviour
{
    public void setGameMode(int mode)
    {
        GameSession.mode = (GameMode)mode;
    }
    public void cancelGameStart()
    {
        GameEventMessage.SendEvent("LeaveGameSelection");
    }
}
