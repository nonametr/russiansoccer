﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SettingsManager : MonoBehaviour
{
    public List<OptionSelector> options = new List<OptionSelector>();
    
    void Awake()
    {
        for (int selectorId = 0; selectorId < options.Count; ++selectorId)
        {
            int thisSelectorId = selectorId;

            OptionSelector optionSelector = options[selectorId];
            optionSelector.onOptionUpdate.AddListener((OptionData optionData) => { onOptionChanged(thisSelectorId, optionData); });

            string optionName = optionSelector.transform.parent.gameObject.name;
            string optionValue = PlayerPrefs.GetString(optionName, "");
            if(optionValue.Length > 0)
            {
                OptionData optionData = JsonUtility.FromJson<OptionData>(optionValue);
                optionSelector.deserializeOptionData(optionData);
            }
            else
            {
                optionSelector.loadDefaults();
            }
        }
    }
    public void loadDefaults()
    {
        foreach(OptionSelector option in options)
        {
            option.loadDefaults();
        }
        SoundSwitch.loadDefaults();
    }
    public void onOptionChanged(int selectorId, OptionData optionData)
    {
        OptionSelector optionSelector = options[selectorId];
        string optionName = optionSelector.transform.parent.gameObject.name;
        PlayerPrefs.SetString(optionName, JsonUtility.ToJson(optionData));
    }

    public void findOptionsInChilds()
    {
        options.Clear();
        OptionSelector[] childs = GetComponentsInChildren<OptionSelector>();
        foreach (OptionSelector child in childs)
        {
            options.Add(child);
        }
    }
    public static StatRecordData readStatRecord(int difficulty, int board_size)
    {
        StatRecordData statRecordData;
        string json_data = PlayerPrefs.GetString("d" + difficulty + "_b" + board_size, "");
        if (json_data.Length > 0)
        {
            statRecordData = JsonUtility.FromJson<StatRecordData>(json_data);
        }
        else
        {
            statRecordData = new StatRecordData();
        }
        return statRecordData;
    }
    public static void writeStatRecord(int difficulty, int board_size, StatRecordData statRecordData)
    {
        PlayerPrefs.SetString("d" + difficulty + "_b" + board_size, JsonUtility.ToJson(statRecordData));
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(SettingsManager))]
public class SettingsManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SettingsManager myScript = (SettingsManager)target;
        if (GUILayout.Button("Find options in childs"))
        {
            myScript.findOptionsInChilds();
        }
        if (GUILayout.Button("PlayerPrefs.DeleteAll"))
        {
            PlayerPrefs.DeleteAll();
        }
    }
}
#endif