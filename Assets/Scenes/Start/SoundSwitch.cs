﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine;

public class SoundSwitch : MonoBehaviour
{
    private void Awake()
    {
        AudioListener.volume = PlayerPrefs.GetFloat("volume", 1);
    }
    public void doSoundSwitch()
    {        
        AudioListener.volume = AudioListener.volume > 0 ? 0 : 1;
        PlayerPrefs.SetFloat("volume", AudioListener.volume);
    }
    public static void loadDefaults()
    {
        AudioListener.volume = 1;
        PlayerPrefs.SetFloat("volume", AudioListener.volume);
    }
}
