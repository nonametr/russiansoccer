﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Linq;

[System.Serializable]
public class OptionData
{
    public List<int> values = new List<int>();
}
[System.Serializable]
public class OptionChangeEvent : UnityEvent<OptionData>
{
}
public class OptionSelector : MonoBehaviour
{
    public enum Mode { SINGLE, SINGLE_OPTIONAL, MULTIPLE_OPTIONAL, PROGRESSION };
    public Mode mode;

    public List<GameObject> options = new List<GameObject>();
    public List<int> defaults = new List<int>() { 0 };
    public List<Color> active = new List<Color>();
    public List<Color> inactive = new List<Color>();

    public OptionChangeEvent onOptionUpdate;
    
    void Start()
    {
        for (int optionId = 0; optionId < options.Count; ++optionId)
        {
            int thisOptionId = optionId;
            Button optButton = options[optionId].GetComponent<Button>();
            optButton.onClick.AddListener(() => { onOptionSelect(thisOptionId);  });
        }
    }

    public void onOptionSelect(int optionId)
    {
        Image optionImg = options[optionId].GetComponent<Image>();

        switch (mode)
        {
            case Mode.SINGLE:
                inactivateAll();
                optionImg.color = active[optionId] == optionImg.color ? inactive[optionId] : active[optionId];
                break;
            case Mode.SINGLE_OPTIONAL:
                Color optionColor = optionImg.color;
                inactivateAll();
                optionImg.color = active[optionId] == optionColor ? inactive[optionId] : active[optionId];
                break;
            case Mode.MULTIPLE_OPTIONAL:
                optionImg.color = active[optionId] == optionImg.color ? inactive[optionId] : active[optionId];
                break;
            case Mode.PROGRESSION:
                for(int i = 0; i <= optionId; ++i)
                {
                    options[i].GetComponent<Image>().color = active[optionId];
                }
                for (int i = optionId + 1; i < options.Count; ++i)
                {
                    options[i].GetComponent<Image>().color = inactive[optionId];
                }
                break;
        }

        onOptionUpdate.Invoke(serializeOptionData());
    }
    public void loadDefaults()
    {
        inactivateAll();
        foreach (int defaultVal in defaults)
        {
            onOptionSelect(defaultVal);
        }
    }
    public void inactivateAll()
    {
        for (int i = 0; i < options.Count; ++i)
        {
            options[i].GetComponent<Image>().color = inactive[i];
        }
    }
    public void deserializeOptionData(OptionData optionData)
    {
        inactivateAll();
        foreach (int optionId in optionData.values)
        {
            onOptionSelect(optionId);
        }
    }
    public OptionData serializeOptionData()
    {
        OptionData optionData = new OptionData();
        for (int i = 0; i < options.Count; ++i)
        {
            if (options[i].GetComponent<Image>().color == active[i])
            {
                optionData.values.Add(i);
            }
        }
        return optionData;
    }

    public void findOptionsInChilds()
    {
        options.Clear();
        active.Clear();
        inactive.Clear();
        Button[] childs = GetComponentsInChildren<Button>();
        foreach(Button child in childs)
        {
            options.Add(child.gameObject);
            active.Add(Color.green);
            inactive.Add(Color.white);
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(OptionSelector))]
public class OptionSelectorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        OptionSelector myScript = (OptionSelector)target;
        if (GUILayout.Button("Find options in childs"))
        {
            myScript.findOptionsInChilds();
        }
    }
}
#endif