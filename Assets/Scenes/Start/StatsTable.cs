﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using I2.Loc;
using TMPro;

[System.Serializable]
public class StatRecordData
{
    public int redWins;
    public int redGames;
    public int blueWins;
    public int blueGames;
};
public class StatsTable : MonoBehaviour
{
    public GameObject __templateRecord;

    public Dictionary<int, string> difficultyOptions = new Dictionary<int, string>()
    {
        { 0, "Settings/easy" },
        { 1, "Settings/advanced" },
        { 2, "Settings/hard" }
    };

    public Dictionary<int, string> boardSizeOptions = new Dictionary<int, string>()
    {
        { 0, "Settings/normal" },
        { 1, "Settings/large" },
        { 2, "Settings/huge" }
    };

    void Start()
    {
        loadStats();
    }
    public void loadStats()
    {
        foreach (var boardSizeKV in boardSizeOptions)
        {
            foreach (var difficultyKV in difficultyOptions)
            {
                string stat = PlayerPrefs.GetString("d" + difficultyKV.Key + "_b" + boardSizeKV.Key, "");
                if (stat.Length != 0)
                {
                    GameObject go = Instantiate(__templateRecord);
                    go.GetComponent<RectTransform>().Rotate(Vector3.back, -28.323f);
                    StatRecord rec = go.GetComponent<StatRecord>();

                    Localize lz = rec.difficultyGO.GetComponent<Localize>();
                    
                    rec.difficultyGO.GetComponent<Localize>().SetTerm(difficultyKV.Value);
                    rec.boardSizeGO.GetComponent<Localize>().SetTerm(boardSizeKV.Value);

                    go.transform.SetParent(transform);
                    go.SetActive(true);

                    StatRecordData data = JsonUtility.FromJson<StatRecordData>(stat);
                    rec.RedGO.GetComponent<TextMeshProUGUI>().SetText(data.redWins + "/" + data.redGames);
                    rec.BlueGO.GetComponent<TextMeshProUGUI>().SetText(data.blueWins + "/" + data.blueGames);
                }
            }
        }
    }
    public void resetStats()
    {
        foreach (var boardSizeKV in boardSizeOptions)
        {
            foreach (var difficultyKV in difficultyOptions)
            {
                PlayerPrefs.SetString("d" + difficultyKV.Key + "_" + "b" + boardSizeKV.Key, "");
            }
        }
        List<GameObject> cleanupList = new List<GameObject>();
        foreach (Transform child in transform)
        {
            cleanupList.Add(child.gameObject);
        }

        foreach (GameObject go in cleanupList)
        {
            Destroy(go);
        }

        loadStats();
    }
}
