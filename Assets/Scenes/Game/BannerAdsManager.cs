﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;

public class BannerAdsManager : MonoBehaviour
{
#if UNITY_IPHONE
    public static string gameId = "3177235";
#elif UNITY_ANDROID
    public static string gameId = "3205104";
#else 
    public static string gameId = "something else";
#endif

    public string placementId = "bannerPlacement";

    void Awake()
    {
        Advertisement.Initialize(gameId, false);
        //StartCoroutine(showBannerWhenReady());
    }
    IEnumerator showBannerWhenReady()
    {
        while (!Advertisement.IsReady(placementId))
        {
            yield return new WaitForSeconds(0.5f);
        }
        Advertisement.Banner.Show(placementId);
    }
}