﻿using UnityEngine;
using System;
using Doozy.Engine.Soundy;

[RequireComponent(typeof(BoxCollider2D))]
public class PossibleTurnPoint : MonoBehaviour
{
    public static event Action<Step> onPlayerStepTry = (Step step) => { };

    public Step step;
    public SpriteRenderer sprite;

    private void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
    }
    private void OnDestroy()
    {
        onPlayerStepTry = (Step step) => { };
    }
    public void OnMouseUp()
    {
        moveInto();
    }
    public void moveInto()
    {
        if (Time.fixedTime - Ball.lastMove < 0.35f)
            return;

        Ball.lastMove = Time.fixedTime;
        if (gameObject.activeSelf && sprite.color != Ball.inst.impossibleTurn)
        {
            SoundyManager.Play(Ball.inst.stepSound, Ball.inst.audioGroup);
            onPlayerStepTry.Invoke(step);
        }
        else
        {
            SoundyManager.Play(Ball.inst.impossibleStepSound, Ball.inst.audioGroup);
        }
    }

    public Point getPoint()
    {
        Vector2Int pos = PointRemap.stepToPos(Ball.pos, step);
        return Ball.inst.game.board.points[pos];
    }
}
