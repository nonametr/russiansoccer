﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Doozy.Engine.Soundy;

[RequireComponent(typeof(BoxCollider2D))]
public class CurrentPoint : MonoBehaviour
{
    public static event Action onPlayerTurnCommit = () => { };

    private void OnDestroy()
    {
        onPlayerTurnCommit = () => { };
    }
    public void OnMouseDown()
    {
        SoundyManager.Play(Ball.inst.commitSound, Ball.inst.audioGroup);
        onPlayerTurnCommit.Invoke();
    }
}
