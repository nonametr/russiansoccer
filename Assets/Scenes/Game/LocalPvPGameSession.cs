﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalPvPGameSession : MonoBehaviour
{
    private GameSession game;

    private Player[] player = new Player[] { null, null, null };

    void Start()
    {
        game = GetComponent<GameSession>();

        StartCoroutine(gameLoop());
    }
    IEnumerator gameLoop()
    {
        bool gameInProgress = game.isInProgress();
        player[1] = new SelfPlayer(1, game);
        player[2] = new SelfPlayer(2, game);
        while (gameInProgress)
        {
            int playerId = game.activePlayerId();
            Ball.pos = game.getBallPos();

            yield return player[playerId].waitForTurn((Step step) =>
            {
                int result = GameEngine.mcts_ai_do_step(GameSession.handle, step);

                gameInProgress = game.isInProgress();
                if (gameInProgress)
                {
                    game.drawTurn(Ball.pos, game.getBallPos(), step, playerId);
                }
                else
                    return;
            });
        }
    }
}
