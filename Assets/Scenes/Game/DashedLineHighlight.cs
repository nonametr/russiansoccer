﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashedLineHighlight : MonoBehaviour
{
    public Material normalMat;
    public Color normalColor;
    public bool reverse = false;
    public Vector2 offset = Vector2.zero;
    public LineRenderer lr;

    void Start()
    {
        lr = GetComponent<LineRenderer>();
        Board board = GetComponentInParent<Board>();

        lr.material = board.dashedLineMaterial;
        if(Vector3.Distance(lr.GetPosition(0), lr.GetPosition(1)) > 1.0f)
        {
            lr.material.SetTextureScale("_MainTex", new Vector2(2 * Mathf.Sqrt(2), 0));
        }
    }

    public void setReverse(bool val)
    {
        reverse = val;
    }

    void Update()
    {
        offset.x += reverse ? -Time.deltaTime : Time.deltaTime;
        lr.material.SetTextureOffset("_MainTex", offset);
    }
}
