using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public enum StateStatus
{
    IN_PROGRESS = 0,
    WIN_1,
    WIN_2
};

public class GameEngine
{
#if UNITY_IPHONE && !UNITY_EDITOR//On iOS plugins are statically linked into the executable
    [DllImport("__Internal")] public static extern int state_get_ball(IntPtr state);
    [DllImport("__Internal")] public static extern int state_get_active(IntPtr state);
    [DllImport("__Internal")] public static extern StateStatus state_status(IntPtr state);
    [DllImport("__Internal")] public static extern int mcts_ai_undo_step(IntPtr ai);
    [DllImport("__Internal")] public static extern IntPtr mcts_ai_get_state(IntPtr ai);
    [DllImport("__Internal")] public static extern IntPtr create_ai_storage();
    [DllImport("__Internal")] public static extern void destroy_ai_storage(IntPtr ai);
    [DllImport("__Internal")] public static extern int init_mcts_ai(IntPtr ai, IntPtr geometry);
    [DllImport("__Internal")] public static extern IntPtr create_std_geometry(int width, int height, int goal_width, int penalty_len);
    [DllImport("__Internal")] public static extern void destroy_geometry(IntPtr geometry);
    [DllImport("__Internal")] public static extern int mcts_ai_do_step(IntPtr ai, Step step);
    [DllImport("__Internal")] public static extern int mcts_ai_set_param(IntPtr ai, string name, ref int value);
    [DllImport("__Internal")] public static extern Step mcts_ai_go(IntPtr ai, IntPtr stats);
    [DllImport("__Internal")] public static extern void free_mcts_ai(IntPtr ai);
#else
    [DllImport("librussian-paper-football-engine")] public static extern int state_get_ball(IntPtr state);
    [DllImport("librussian-paper-football-engine")] public static extern int state_get_active(IntPtr state);
    [DllImport("librussian-paper-football-engine")] public static extern StateStatus state_status(IntPtr state);
    [DllImport("librussian-paper-football-engine")] public static extern int mcts_ai_undo_step(IntPtr ai);
    [DllImport("librussian-paper-football-engine")] public static extern IntPtr mcts_ai_get_state(IntPtr ai);
    [DllImport("librussian-paper-football-engine")] public static extern IntPtr create_ai_storage();
    [DllImport("librussian-paper-football-engine")] public static extern void destroy_ai_storage(IntPtr ai);
    [DllImport("librussian-paper-football-engine")] public static extern int init_mcts_ai(IntPtr ai, IntPtr geometry);
    [DllImport("librussian-paper-football-engine")] public static extern IntPtr create_std_geometry(int width, int height, int goal_width, int penalty_len);
    [DllImport("librussian-paper-football-engine")] public static extern void destroy_geometry(IntPtr geometry);
    [DllImport("librussian-paper-football-engine")] public static extern int mcts_ai_do_step(IntPtr ai, Step step);
    [DllImport("librussian-paper-football-engine")] public static extern int mcts_ai_set_param(IntPtr ai, string name, ref int value);
    [DllImport("librussian-paper-football-engine")] public static extern Step mcts_ai_go(IntPtr ai, IntPtr stats);
    [DllImport("librussian-paper-football-engine")] public static extern void free_mcts_ai(IntPtr ai);
#endif


}
