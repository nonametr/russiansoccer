﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using System.Linq;

public interface Player
{
    IEnumerator waitForTurn(Action<Step> callback);
}

public class AIPlayer : Player
{
    private GameSession game;
    public AIPlayer(GameSession game)
    {
        this.game = game;
    }
    public IEnumerator waitForTurn(Action<Step> callback)
    {
        yield return new WaitForSeconds(0.25f);
        Step aiStep = Step.QSTEPS;
        Task aiTask = Task.Run(() =>
        {
            aiStep = GameEngine.mcts_ai_go(GameSession.handle, IntPtr.Zero);
        });

        while (aiTask.IsCompleted == false)
        {
            yield return null;
        }

        callback(aiStep);
    }
}

public class SelfPlayer : Player
{
    private int selfId;
    private List<Vector2Int> history = new List<Vector2Int>();
    private Stack<Step> commitStack = new Stack<Step>();
    private GameSession game;
    private int commitSize = 0;

    private readonly Step[] possibleSteps;
    public SelfPlayer(int playerId, GameSession game)
    {
        this.selfId = playerId;
        this.game = game;
        this.possibleSteps = ((Step[])Enum.GetValues(typeof(Step))).Where(step => step != Step.QSTEPS).ToArray();
    }
    public IEnumerator waitForTurn(Action<Step> callback)
    {
        hlEnable();
        CurrentPoint.onPlayerTurnCommit += onPlayerTurnCommit;
        PossibleTurnPoint.onPlayerStepTry += onPlayerStepTry;
        TimeLimitController.inst.startCountDown();
        while (commitStack.Count == 0)
        {
            yield return null;
        }
        TimeLimitController.inst.endCountdown();
        callback(commitStack.Pop());
        PossibleTurnPoint.onPlayerStepTry -= onPlayerStepTry;
        CurrentPoint.onPlayerTurnCommit -= onPlayerTurnCommit;
        hlDisable();
    }
    public bool isMyTurn()
    {
        return game.activePlayerId() == selfId;
    }
    private void onPlayerTurnCommit()
    {
        if (history.Count == 0)
            return;

        if (isMyTurn())
        {
            commitSize += history.Count;
        }
        else
        {
            commitSize = 0;
        }
        
        Vector2Int toPos = Ball.pos;
        for (int i = history.Count - 1; i >= 0; --i)
        {
            GameEngine.mcts_ai_undo_step(GameSession.handle);
            Vector2Int fromPos = history[i];
            Step step = PointRemap.posToStep(fromPos, toPos);
            commitStack.Push(step);
            toPos = fromPos;
        }
        Ball.pos = history[0];
        history.Clear();
    }    
    public void addTryLine(Vector2Int from, Vector2Int to, Step step)
    {
        while(!Vector2Int.Equals(from, to))
        {
            Vector2Int targetPos = PointRemap.stepToPos(from, step);

            LineRenderer lr;
            game.board.lines.TryGetValue(new Line(from, targetPos), out lr);

            DashedLineHighlight dline = lr.gameObject.AddComponent<DashedLineHighlight>();

            dline.normalMat = lr.sharedMaterial;
            dline.normalColor = lr.startColor;
            if (Vector3.Equals(lr.GetPosition(0), new Vector3(from.x, from.y, 0)))
            {
                dline.setReverse(true);
            }

            lr.gameObject.SetActive(true);
            lr.startColor = game.playerColors[selfId];
            lr.endColor = game.playerColors[selfId];

            from = targetPos;
        }
    }
    public void delTryLine(Vector2Int from, Vector2Int to, Step step)
    {
        while (!Vector2Int.Equals(from, to))
        {
            Vector2Int targetPos = PointRemap.stepToPos(from, step);

            LineRenderer lr;
            game.board.lines.TryGetValue(new Line(from, targetPos), out lr);
            DashedLineHighlight dline = lr.gameObject.GetComponent<DashedLineHighlight>();

            lr.startColor = dline.normalColor;
            lr.endColor = dline.normalColor;
            lr.sharedMaterial = dline.normalMat;

            if (Vector3.Distance(lr.GetPosition(0), lr.GetPosition(1)) > 1.0f && dline.normalColor == Color.white)
            {
                lr.gameObject.SetActive(false);
            }

            GameObject.Destroy(dline);
            from = targetPos;
        }
    }
    private bool isRevertTurn(Step step)
    {
        PossibleTurnPoint possibleTurnPoint = Ball.inst.stepPoints[step];
        SpriteRenderer sprite = possibleTurnPoint.GetComponent<SpriteRenderer>();
        return sprite.color == Ball.inst.revertTurn;
    }
    public void onPlayerStepTry(Step step)
    {
        Vector2Int fromPos = Ball.pos;
        Vector2Int toPos = PointRemap.stepToPos(fromPos, step);

        if (isRevertTurn(step))
        {
            history.RemoveAt(history.Count - 1);
            GameEngine.mcts_ai_undo_step(GameSession.handle);
            Ball.pos = game.getBallPos();

            delTryLine(fromPos, Ball.pos, step);
        }
        else
        {
            history.Add(Ball.pos);
            GameEngine.mcts_ai_do_step(GameSession.handle, step);
            if (game.isInProgress())
            {
                Ball.pos = game.getBallPos();
                addTryLine(fromPos, Ball.pos, step);
            }
            else
                return;
        }

        if (isMyTurn())
        {
            hlEnable();
        }
        else
        {
            hlDisable();
        }
        hlPrev();
    }
    private void hlPrev()
    {
        if (history.Count == 0)
            return;

        Vector2Int prevPos = history[history.Count - 1];
        Step toPrevStep = PointRemap.posToStep(Ball.pos, prevPos);

        PossibleTurnPoint possibleTurnPoint = Ball.inst.stepPoints[toPrevStep];
        possibleTurnPoint.gameObject.SetActive(true);
        possibleTurnPoint.sprite.color = Ball.inst.revertTurn;
    }
    private bool isPenalty()
    {
        return (commitSize + history.Count) >= 3;
    }
    private void hlEnable()
    {
        foreach (Step step in possibleSteps)
        {
            int result = GameEngine.mcts_ai_do_step(GameSession.handle, step);
            PossibleTurnPoint possibleTurnPoint = Ball.inst.stepPoints[step];
            SpriteRenderer sprite = possibleTurnPoint.GetComponent<SpriteRenderer>();
            sprite.gameObject.SetActive(true);
            if (result != 0)
            {
                sprite.color = Ball.inst.impossibleTurn;
            }
            else
            {
                if (isPenalty())
                {
                    sprite.color = Ball.inst.penaltyTurn;
                }
                else
                {
                    sprite.color = Ball.inst.possibleTurn;
                }
                GameEngine.mcts_ai_undo_step(GameSession.handle);
            }
        }
    }
    private void hlDisable()
    {
        foreach (Step step in possibleSteps)
        {
            PossibleTurnPoint possibleTurnPoint = Ball.inst.stepPoints[step];
            SpriteRenderer sprite = possibleTurnPoint.GetComponent<SpriteRenderer>();
            sprite.gameObject.SetActive(false);
        }
    }
}

public class NetworkPlayer : Player
{
    public IEnumerator waitForTurn(Action<Step> callback)
    {
        yield return null;
    }
}