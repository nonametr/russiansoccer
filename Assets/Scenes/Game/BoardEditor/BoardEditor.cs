﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class BoardData
{
    public Vector2Int size;
    public List<PointType> points = new List<PointType>();
}
public class BoardEditor : MonoBehaviour
{
    private Dictionary<Vector2Int, PointEditor> points = new Dictionary<Vector2Int, PointEditor>();

    public string importString;
    public GameObject pointPrefab;
    public int width = 9;
    public int length = 11;
    public GameObject boardPrefab;

    public void buildEditor()
    {
        clear();

        Camera.main.orthographicSize = Mathf.FloorToInt((width + length) * 0.5f);
        Camera.main.transform.position = new Vector3(width * 0.45f, length * 0.5f, -10);

        for (int x = 0; x < width; ++x)
        {
            for (int y = -1; y < length + 1; ++y)
            {
                GameObject pointGO = Instantiate(pointPrefab);
                pointGO.transform.position = new Vector3(x, y, -7);
                pointGO.transform.parent = transform;
                pointGO.name = "Point_" + x + "_" + y;
                pointGO.SetActive(true);

                PointEditor point = pointGO.GetComponent<PointEditor>();
                point.data.pos = new Vector2Int(x, y);

                if (x == 0 || y == 0 || x == width - 1 || y == length - 1)
                {
                    point.data.type = PointType.BORDER;
                }

                if (y == -1 || y == length)
                {
                    point.data.type = PointType.GOAL;
                }

                point.updateColor();

                points.Add(point.data.pos, point);
            }
        }
    }
    public void clear()
    {
        points.Clear();

        List<GameObject> cleanupList = new List<GameObject>();
        foreach (Transform child in transform)
        {
            if (!child.name.Contains("template"))
            {
                cleanupList.Add(child.gameObject);
            }
        }

        foreach (GameObject go in cleanupList)
        {
            DestroyImmediate(go);
        }
    }
    public BoardData getBoardData()
    {
        BoardData boardData = new BoardData() { size = new Vector2Int(width, length) };

        foreach (Transform child in transform)
        {
            if (!child.name.Contains("template"))
            {
                PointEditor pe = child.gameObject.GetComponent<PointEditor>();
                boardData.points.Add(pe.data.type);
            }
        }
        return boardData;
    }
    public void publish()
    {
        importString = JsonUtility.ToJson(getBoardData());
        Debug.Log(importString);
    }
    public void import()
    {
        if (importString.Length > 0)
        {
            clear();

            string boardJson = importString.Replace("'", "\"");
            BoardData board = JsonUtility.FromJson<BoardData>(boardJson);

            width = board.size.x;
            length = board.size.y;

            Camera.main.orthographicSize = Mathf.FloorToInt((width + length) * 0.5f);
            Camera.main.transform.position = new Vector3(width * 0.45f, length * 0.5f + 1, -10);

            for (int x = 0; x < width; ++x)
            {
                for (int y = -1; y < length + 1; ++y)
                {
                    PointType type = board.points[x * (length + 2) + (y + 1)];

                    GameObject pointGO = Instantiate(pointPrefab);
                    pointGO.transform.position = new Vector3(x, y, -7);
                    pointGO.transform.parent = transform;
                    pointGO.name = "Point_" + x + "_" + y;
                    pointGO.SetActive(true);

                    PointEditor point = pointGO.GetComponent<PointEditor>();
                    point.data.pos = new Vector2Int(x, y);

                    point.data.type = type;
                    point.updateColor();
                }
            }
        }
    }

    public void buildBoard()
    {
        BoardData boardData = getBoardData();

        GameObject boardGO = Instantiate(boardPrefab);
        boardGO.name = boardData.size.x + "x" + boardData.size.y + "_board";
        boardGO.transform.parent = transform.parent;
        boardGO.SetActive(true);

        Board board = boardGO.GetComponent<Board>();
        board.width = boardData.size.x;
        board.length = boardData.size.y;

        for (int x = 0; x < board.width; ++x)
        {
            for (int y = -1; y < board.length + 1; ++y)
            {
                PointType type = boardData.points[x * (board.length + 2) + (y + 1)];

                if (type != PointType.REMOVE)
                {
                    GameObject pointGO = Instantiate(board.pointPrefab);
                    pointGO.transform.position = new Vector3(x, y, -1);
                    pointGO.transform.parent = boardGO.transform;
                    pointGO.name = "Point_" + x + "_" + y;
                    pointGO.SetActive(true);

                    if (type == PointType.BORDER)
                        pointGO.GetComponent<SpriteRenderer>().color = Color.clear;

                    Point pointBehaviour = pointGO.GetComponent<Point>();
                    pointBehaviour.pos = new Vector2Int(x, y);
                    pointBehaviour.owner = board;
                    pointBehaviour.pointType = type;

                    board.points.Add(new Vector2Int(x, y), pointBehaviour);
                }
            }
        }

        Dictionary<Step, Vector2Int> neighbourMap = new Dictionary<Step, Vector2Int>
        {
            { Step.NORTH, new Vector2Int(0, 1) },
            { Step.NORTH_EAST, new Vector2Int(1, 1) },
            { Step.EAST, new Vector2Int(1, 0) },
            { Step.SOUTH_EAST, new Vector2Int(1, -1) },
            { Step.SOUTH, new Vector2Int(0, -1) },
            { Step.SOUTH_WEST, new Vector2Int(-1, -1) },
            { Step.WEST, new Vector2Int(-1, 0) },
            { Step.NORTH_WEST, new Vector2Int(-1, 1) }
        };
        foreach (KeyValuePair<Vector2Int, Point> kv in board.points)
        {
            foreach (KeyValuePair<Step, Vector2Int> neighbourKV in neighbourMap)
            {
                Point thisPoint = kv.Value;
                Point neighbourPoint;

                Vector2Int neighbourPos = kv.Key + neighbourKV.Value;

                board.points.TryGetValue(neighbourPos, out neighbourPoint);
                if (neighbourPoint)
                {
                    kv.Value.connections.Add(neighbourKV.Key, neighbourPoint);
                    LineRenderer line;
                    if (!board.lines.TryGetValue(new Line(kv.Key, neighbourPos), out line))
                    {
                        GameObject lineGO = Instantiate(board.linePrefab);
                        lineGO.transform.parent = boardGO.transform;
                        lineGO.name = "Line_" + kv.Key + "-" + neighbourPos;
                        if (Vector2Int.Distance(kv.Key, neighbourPos) > 1)
                        {
                            lineGO.SetActive(false);
                        }
                        else
                        {
                            lineGO.SetActive(true);
                        }

                        LineRenderer lr = lineGO.GetComponent<LineRenderer>();
                        board.lines.Add(new Line(kv.Key, neighbourPos), lr);
                        lr.SetPositions(new Vector3[] { new Vector3(kv.Key.x, kv.Key.y, 0), new Vector3(neighbourPos.x, neighbourPos.y, 0) });

                        if (neighbourPoint.pointType == PointType.BORDER && thisPoint.pointType == PointType.BORDER)
                        {
                            lr.material = board.borderLineMaterial;

                        }
                        else if (neighbourPoint.pointType == PointType.GOAL && thisPoint.pointType == PointType.GOAL)
                        {
                            if (thisPoint.pos.y == -1)
                            {
                                lr.material = board.redGoalLineMaterial;
                            }
                            else
                            {
                                lr.material = board.blueGoalLineMaterial;
                            }
                        }
                        else
                        {
                            lr.material = board.normalLineMaterial;
                        }
                    }
                }
            }
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(BoardEditor))]
public class BoardEditorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        BoardEditor builder = (BoardEditor)target;
        if (GUILayout.Button("Publish"))
        {
            builder.publish();
        }
        if (GUILayout.Button("BuildBoard"))
        {
            builder.buildBoard();
        }
        if (GUILayout.Button("BuildEditor"))
        {
            builder.buildEditor();
        }
        if (GUILayout.Button("Clear"))
        {
            builder.clear();
        }
        if (GUILayout.Button("Import"))
        {
            builder.import();
        }
    }
}
#endif