﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PointData
{
    public PointType type = PointType.PLAYABLE;
    public Vector2Int pos;
}
[System.Serializable]
public enum PointType { REMOVE, PLAYABLE, BORDER, GOAL, ACTIVE };
public class PointEditor : MonoBehaviour
{
    public PointData data = new PointData();

    public readonly static Dictionary<PointType, Color> pointStateColor = new Dictionary<PointType, Color>
    {
        { PointType.REMOVE, new Color(0, 0, 0, 0.0f) },
        { PointType.PLAYABLE, new Color(0, 0, 0, 1.0f) },
        { PointType.BORDER, new Color(1, 1, 1, 1.0f) },
        { PointType.GOAL, new Color(1, 0, 0, 1.0f) },
        { PointType.ACTIVE, new Color(0, 0, 0, 1.0f) }
    };

    private void Start()
    {
        updateColor();
    }

    public void updateColor()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        sr.color = pointStateColor[data.type];
    }
    private void OnMouseOver()
    {
        if (Input.GetMouseButtonUp(0))
        {
            data.type = (data.type >= PointType.GOAL) ? PointType.REMOVE : data.type + 1;
            updateColor();
        }
        else if (Input.GetMouseButtonUp(1))
        {
            data.type = (data.type <= PointType.REMOVE) ? PointType.GOAL : data.type - 1;
            updateColor();
        }
    }
}
