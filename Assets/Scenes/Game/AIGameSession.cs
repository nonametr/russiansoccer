﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.Threading.Tasks;

public enum Difficulty
{
    Easy,
    Normal,
    Hard
}

public enum BoardSize
{
    Small,
    Middle,
    Large
}

public class AIGameSession : MonoBehaviour
{
    public int selfId = 1;
    private GameSession game;

    private Player[] player = new Player[] { null, null, null};

    void Start()
    {
        game = GetComponent<GameSession>();

        changeDifficulty();
        StartCoroutine(gameLoop());
    }
    public void changeDifficulty()
    {
        float[] qthinkOptions = { 0.125f, 1.0f, 4, 12, 16 };
        float[] cacheOptions = { 0.5f, 4, 16, 32, 48 };

        int qthink = Mathf.FloorToInt(qthinkOptions[game.difficulty] * 1024.0f * 1024.0f);
        int cache = Mathf.FloorToInt(cacheOptions[game.difficulty] * 1024.0f * 1024.0f);

        GameEngine.mcts_ai_set_param(GameSession.handle, "qthink", ref qthink);
        GameEngine.mcts_ai_set_param(GameSession.handle, "cache", ref cache);
    }
    public IEnumerator doAITurn(Action<Step> callback)
    {
        yield return new WaitForSeconds(0.25f);
        Step aiStep = Step.QSTEPS;
        Task aiTask = Task.Run(() =>
        {
            aiStep = GameEngine.mcts_ai_go(GameSession.handle, IntPtr.Zero);
        });

        while (aiTask.IsCompleted == false)
        {
            yield return null;
        }

        callback(aiStep);
    }
    void createPlayers(int firstTurn)
    {
        switch (firstTurn)
        {
            case 0:
                selfId = 1;
                player[1] = new SelfPlayer(1, game);
                player[2] = new AIPlayer(game);
                break;
            case 1:
                selfId = 2;
                player[1] = new AIPlayer(game);
                player[2] = new SelfPlayer(2, game);
                break;
            case 2:
                firstTurn = UnityEngine.Random.Range(0, 2);
                createPlayers(firstTurn);
                return;
        }

        //--------------STATS BEGIN-----------------
        StatRecordData statRecord = SettingsManager.readStatRecord(game.difficulty, game.boardSize);

        RankCalc.onNewGame(game.difficulty, game.boardSize);
        int game_id = PlayerPrefs.GetInt("ai_game_id", 1);
        PlayerPrefs.SetInt("ai_game_id", game_id + 1);

        if (selfId == 1)
            statRecord.redGames++;
        else
            statRecord.blueGames++;

        SettingsManager.writeStatRecord(game.difficulty, game.boardSize, statRecord);
        //--------------STATS END-----------------
    }
    IEnumerator gameLoop()
    {
        string firstTurnStr = PlayerPrefs.GetString("FirstTurn");
        OptionData firstTurnData = JsonUtility.FromJson<OptionData>(firstTurnStr);
        int firstTurn = firstTurnData.values[0];

        bool gameInProgress = game.isInProgress();
        createPlayers(firstTurn);
        while (gameInProgress)
        {
            int playerId = game.activePlayerId();
            Ball.pos = game.getBallPos();

            yield return player[playerId].waitForTurn((Step step) =>
            {
                int result = GameEngine.mcts_ai_do_step(GameSession.handle, step);

                gameInProgress = game.isInProgress();
                if (gameInProgress)
                {
                    game.drawTurn(Ball.pos, game.getBallPos(), step, playerId);
                }
                else
                    return;
            });
        }
    }
}
