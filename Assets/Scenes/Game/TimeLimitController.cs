﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine;
using Doozy.Engine.Soundy;

public class TimeLimitController : MonoBehaviour
{
    private Dictionary<int, float> timeLimitRemap = new Dictionary<int, float>()
    {
        { 0, 15 },
        { 1, 60 },
        { 2, 180 }
    };
    public float defaultTimeLimit;
    public AudioClip warningSound;

    public float timeProgress;
    public Animator animator;
    public event System.Action outOfTimeEvent;

    private static TimeLimitController _inst;
    public static TimeLimitController inst
    {
        get
        {
            if(_inst == null)
            {
                _inst = FindObjectOfType<TimeLimitController>();
            }
            return _inst;
        }
    }
    private int getTimeLimitOption()
    {
        string timeLimitOpt = PlayerPrefs.GetString("TimeLimit", "");
        if (timeLimitOpt.Length <= 0)
            return -1;

        OptionData timeLimitData = JsonUtility.FromJson<OptionData>(timeLimitOpt);

        if (timeLimitData.values.Count <= 0)
            return -1;

        return timeLimitData.values[0];
    }
    void Start()
    {
        animator = GetComponent<Animator>();


        int timeLimitOption = getTimeLimitOption();

        if (timeLimitOption != -1)
        {
            animator.SetFloat("Progress", 0.0f);
            defaultTimeLimit = timeLimitRemap[timeLimitOption];
        }
        else
        {
            animator.SetFloat("Progress", 1.0f);
            defaultTimeLimit = -1;
        }
    }
    public void startCountDown()
    {
        if (defaultTimeLimit == -1)
            return;

        InvokeRepeating("countDown", 0, 0.05f);
    }
    public void endCountdown()
    {
        if (defaultTimeLimit == -1)
            return;

        timeProgress = 0.0f;
        animator.SetFloat("Progress", 0);
        CancelInvoke("countDown");
    }

    public void countDown()
    {
        timeProgress += 0.05f;

        float progress = timeProgress / defaultTimeLimit;
        animator.SetFloat("Progress", progress);

        if(progress > 1.1f)
        {
            outOfTimeEvent?.Invoke();
            CancelInvoke("countDown");
            GameEventMessage.SendEvent("OutOfTime");
        }
    }
}
