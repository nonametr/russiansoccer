﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BoardBuilder : MonoBehaviour
{
    public int width = 5;
    public int length = 5;

    public GameObject boardPrefab;

    public void buildBoard()
    {
        GameObject boardGO = Instantiate(boardPrefab);
        boardGO.name = width + "x" + length + "_board";
        boardGO.transform.parent = transform.parent;
        boardGO.SetActive(true);

        Board board = boardGO.GetComponent<Board>();
        board.width = width;
        board.length = length;

        for (int x = 0; x < width; ++x)
        {
            for (int y = -1; y < length + 1; ++y)
            {
                GameObject pointGO = Instantiate(board.pointPrefab);
                pointGO.transform.position = new Vector3(x, y, -1);
                pointGO.transform.parent = boardGO.transform;
                pointGO.name = "Point_" + x + "_" + y;
                pointGO.SetActive(true);

                Point pointBehaviour = pointGO.GetComponent<Point>();
                pointBehaviour.pos = new Vector2Int(x, y);
                pointBehaviour.owner = board;

                board.points.Add(new Vector2Int(x, y), pointBehaviour);
            }
        }

        Dictionary<Step, Vector2Int> neighbourMap = new Dictionary<Step, Vector2Int>
        {
            { Step.NORTH, new Vector2Int(0, 1) },
            { Step.NORTH_EAST, new Vector2Int(1, 1) },
            { Step.EAST, new Vector2Int(1, 0) },
            { Step.SOUTH_EAST, new Vector2Int(1, -1) },
            { Step.SOUTH, new Vector2Int(0, -1) },
            { Step.SOUTH_WEST, new Vector2Int(-1, -1) },
            { Step.WEST, new Vector2Int(-1, 0) },
            { Step.NORTH_WEST, new Vector2Int(-1, 1) }
        };
        foreach (KeyValuePair<Vector2Int, Point> kv in board.points)
        {
            foreach (KeyValuePair<Step, Vector2Int> neighbourKV in neighbourMap)
            {
                Point thisPoint = kv.Value;
                Point neighbourPoint;

                Vector2Int neighbourPos = kv.Key + neighbourKV.Value;

                board.points.TryGetValue(neighbourPos, out neighbourPoint);
                if (neighbourPoint)
                {
                    kv.Value.connections.Add(neighbourKV.Key, neighbourPoint);
                    LineRenderer line;
                    if (!board.lines.TryGetValue(new Line(kv.Key, neighbourPos), out line))
                    {
                        GameObject lineGO = Instantiate(board.linePrefab);
                        lineGO.transform.parent = boardGO.transform;
                        lineGO.name = "Line_" + kv.Key + "-" + neighbourPos;
                        if (Vector2Int.Distance(kv.Key, neighbourPos) > 1)
                        {
                            lineGO.SetActive(false);
                        }
                        else
                        {
                            lineGO.SetActive(true);
                        }

                        LineRenderer lr = lineGO.GetComponent<LineRenderer>();
                        board.lines.Add(new Line(kv.Key, neighbourPos), lr);
                        lr.SetPositions(new Vector3[] { new Vector3(kv.Key.x, kv.Key.y, 0), new Vector3(neighbourPos.x, neighbourPos.y, 0) });
                    }
                }
            }
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(BoardBuilder))]
public class BoardBuilderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        BoardBuilder myScript = (BoardBuilder)target;
        if (GUILayout.Button("Build"))
        {
            myScript.buildBoard();
        }
    }
}
#endif