﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class LineData
{
    public Vector2Int start;
    public Vector2Int end;
    public int type;//0 - dashed, 1 - normal
    public int color;//0 - white, 1 - red, 2 - blue
}

public class SerializedDeltaBoard
{
    public List<LineData> lines = new List<LineData>();
}

public class Board : MonoBehaviour
{
    public int width;
    public int length;

    public Material dashedLineMaterial;
    public Material normalLineMaterial;
    public Material borderLineMaterial;
    public Material redGoalLineMaterial;
    public Material blueGoalLineMaterial;

    public GameObject linePrefab;
    public GameObject pointPrefab;

    public Dictionary<Line, LineRenderer> lines = new Dictionary<Line, LineRenderer>();
    public Dictionary<Vector2Int, Point> points = new Dictionary<Vector2Int, Point>();

    private Vector2 getNotchRelativeSize()
    {
        return new Vector2((Screen.safeArea.width - Screen.width) / Screen.width, (Screen.height - Screen.safeArea.height) / Screen.height);
    }
    void Start()
    {
        lines.Clear();
        points.Clear();

        Vector2 notchSize = getNotchRelativeSize();

        Camera.main.orthographicSize = Mathf.Max(width / (2 * Camera.main.aspect), (length + 2) * 0.5f) * (1 + notchSize.y);
        Camera.main.transform.position = new Vector3(width * 0.5f - 0.5f, (length - Camera.main.orthographicSize + 0.5f) + Camera.main.orthographicSize * 2 * notchSize.y, -10);

        Point[] childPoints = GetComponentsInChildren<Point>();
        foreach(Point pb in childPoints)
        {
            points.Add(pb.pos, pb);
        }

        LineRenderer[] childLines = GetComponentsInChildren<LineRenderer>(true);
        foreach(LineRenderer lr in childLines)
        {
            Vector3 p0 = lr.GetPosition(0);
            Vector3 p1 = lr.GetPosition(1);
            lines.Add(new Line(new Vector2Int(Mathf.FloorToInt(p0.x), Mathf.FloorToInt(p0.y)), new Vector2Int(Mathf.FloorToInt(p1.x), Mathf.FloorToInt(p1.y))), lr);
        }
    }
    public string serializeBoard()
    {
        SerializedDeltaBoard deltaBoard = new SerializedDeltaBoard();

        Color defaultColor = Color.white;
        foreach(var kv in lines)
        {
            if (kv.Value.startColor != defaultColor)
            {
                LineData lineData = new LineData();
                lineData.start = kv.Key.p1;
                lineData.end = kv.Key.p2;
                if (kv.Value.GetComponent<DashedLineHighlight>() != null)
                {
                    lineData.type = 0;
                }
                else
                {
                    lineData.type = 1;
                }

                lineData.color = kv.Value.startColor == Color.red ? 1 : 2;
                
                deltaBoard.lines.Add(lineData);
            }
        }
        return JsonUtility.ToJson(deltaBoard);
    }
    public void deserializeBoard(string json)
    {
        SerializedDeltaBoard deltaBoard = JsonUtility.FromJson<SerializedDeltaBoard>(json);
        if (deltaBoard == null)
            return;
        
        resetBoard();

        foreach (LineData line in deltaBoard.lines)
        {
            LineRenderer lr;
            if (lines.TryGetValue(new Line(line.start, line.end), out lr))
            {
                lr.gameObject.SetActive(true);
                switch (line.type)
                {
                    case 0:
                        lr.material = dashedLineMaterial;
                        break;
                    case 1:
                        lr.material = normalLineMaterial;
                        break;                       
                }
                switch(line.color)
                {
                    case 0:
                        lr.material.color = Color.white;
                        lr.startColor = Color.white;
                        lr.endColor = Color.white;
                        break;
                    case 1:
                        lr.material.color = Color.red;
                        lr.startColor = Color.red;
                        lr.endColor = Color.red;
                        break;
                    case 2:
                        lr.material.color = Color.blue;
                        lr.startColor = Color.blue;
                        lr.endColor = Color.blue;
                        break;
                }
            }           
        }
    }

    public void resetBoard()
    {
        Dictionary<Step, Vector2Int> neighbourMap = new Dictionary<Step, Vector2Int>
        {
            { Step.NORTH, new Vector2Int(0, 1) },
            { Step.NORTH_EAST, new Vector2Int(1, 1) },
            { Step.EAST, new Vector2Int(1, 0) },
            { Step.SOUTH_EAST, new Vector2Int(1, -1) },
            { Step.SOUTH, new Vector2Int(0, -1) },
            { Step.SOUTH_WEST, new Vector2Int(-1, -1) },
            { Step.WEST, new Vector2Int(-1, 0) },
            { Step.NORTH_WEST, new Vector2Int(-1, 1) }
        };
        foreach (KeyValuePair<Vector2Int, Point> kv in points)
        {
            foreach (KeyValuePair<Step, Vector2Int> neighbourKV in neighbourMap)
            {
                Point thisPoint = kv.Value;
                Point neighbourPoint;

                Vector2Int neighbourPos = kv.Key + neighbourKV.Value;

                points.TryGetValue(neighbourPos, out neighbourPoint);
                if (neighbourPoint)
                {
                    LineRenderer lr;
                    if (lines.TryGetValue(new Line(kv.Key, neighbourPos), out lr))
                    {
                        if (Vector3.Distance(lr.GetPosition(0), lr.GetPosition(1)) > 1.0f)
                        {
                            lr.gameObject.SetActive(false);
                        }
                        else
                        {
                            lr.startColor = Color.white;
                            lr.endColor = Color.white;

                            if (neighbourPoint.pointType == PointType.BORDER && thisPoint.pointType == PointType.BORDER)
                            {
                                lr.material = borderLineMaterial;

                            }
                            else if (neighbourPoint.pointType == PointType.GOAL && thisPoint.pointType == PointType.GOAL)
                            {
                                if (thisPoint.pos.y == -1)
                                {
                                    lr.material = redGoalLineMaterial;
                                }
                                else
                                {
                                    lr.material = blueGoalLineMaterial;
                                }
                            }
                            else
                            {
                                lr.material = normalLineMaterial;
                            }
                        }
                    }
                }
            }
        }
    }
    public void printBoard()
    {
        BoardData boardData = new BoardData() { size = new Vector2Int(width, length) };

        Point[] childPoints = GetComponentsInChildren<Point>();
        foreach (Point pb in childPoints)
        {
            boardData.points.Add(pb.pointType);
        }

        string json = JsonUtility.ToJson(boardData);
        Debug.Log(json);
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(Board))]
public class BoardObjEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Board board = (Board)target;
        if (GUILayout.Button("Print"))
        {
            board.printBoard();
        }
    }
}
#endif