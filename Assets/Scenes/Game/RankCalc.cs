﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RankCalc : MonoBehaviour
{
    void OnEnable()
    {
        float rank = PlayerPrefs.GetFloat("rank", 99);
        GetComponent<TextMeshProUGUI>().SetText(("Top " + (int)rank).ToString());
    }

    public static void onNewGame(int difficulty, int boardSize)
    {
        float rank = PlayerPrefs.GetFloat("rank", 99.0f);
        rank += ((difficulty + boardSize) + 1) * 0.1f;
        rank = Mathf.Clamp(rank, 1, 99);
        PlayerPrefs.SetFloat("rank", rank);
    }

    public static void onWin(int difficulty, int boardSize)
    {
        float rank = PlayerPrefs.GetFloat("rank", 99.0f);
        rank -= 3f*((difficulty + boardSize) + 1) * 0.1f;
        rank = Mathf.Clamp(rank, 1, 99);
        PlayerPrefs.SetFloat("rank", rank);
    }
}
