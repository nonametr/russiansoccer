﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Doozy.Engine.Touchy;

public class Ball : MonoBehaviour
{
    public static float lastMove = 0;

    public GameSession game;
    public CurrentPoint currentPoint;
    public Vector2Int currentPosition = Vector2Int.zero;
    public GameObject[] neighbours = new GameObject[8];

    public AudioMixerGroup audioGroup;

    public AudioClip commitSound;
    public AudioClip stepSound;
    public AudioClip impossibleStepSound;

    public Color revertPenaltyTurn = Color.magenta;
    public Color revertTurn = Color.blue;
    public Color penaltyTurn = Color.yellow;
    public Color possibleTurn = Color.green;
    public Color impossibleTurn = Color.red;
    public float ballAreaLen;

    public Dictionary<Step, PossibleTurnPoint> stepPoints = new Dictionary<Step, PossibleTurnPoint>();
    
    public static Vector2Int pos
    {
        get
        {
            return Ball.inst.currentPosition;
        }
        set
        {
            Ball.inst.transform.position = new Vector3(value.x, value.y, 0);
            Ball.inst.currentPosition = value;
        }
    }
    private static Ball _inst;
    public static Ball inst
    {
        get
        {
            if(_inst == null)
            {
                _inst = FindObjectOfType<Ball>();
            }
            return _inst;
        }
    }
    private void onSwipe(TouchInfo info)
    {
        if (info.Direction == Swipe.None)
            return;

        Step step = PointRemap.swipeToStep(info.Direction);
        stepPoints[step].moveInto();
    }
    private void onLongTap(TouchInfo info)
    {
        currentPoint.OnMouseDown();
    }
    private void OnEnable()
    {
        TouchDetector.Instance.OnLongTapAction += onLongTap;
        TouchDetector.Instance.OnSwipeAction += onSwipe;
    }
    private void OnDisable()
    {
        if (TouchDetector.Instance)
        {
            TouchDetector.Instance.OnLongTapAction -= onLongTap;
            TouchDetector.Instance.OnSwipeAction -= onSwipe;
        }
    }
    private void Awake()
    {
        foreach (GameObject neighbour in neighbours)
        {
            PossibleTurnPoint possibleTurnPoint = neighbour.GetComponent<PossibleTurnPoint>();
            stepPoints.Add(possibleTurnPoint.step, possibleTurnPoint);
        }
    }
    private void Start()
    {
        game = FindObjectOfType<GameSession>();

        Vector3 ballScreenPoint = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 neighbourScreenPoint = Camera.main.WorldToScreenPoint(stepPoints[Step.NORTH_EAST].transform.position);
        ballAreaLen = 3 * Vector3.Distance(ballScreenPoint, neighbourScreenPoint);
    }
    public Point getPoint()
    {
        return game.board.points[currentPosition];
    }
}
