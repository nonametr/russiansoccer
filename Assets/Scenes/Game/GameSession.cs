﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine;

public class GameSession : MonoBehaviour
{
    public Board board;
    public Board[] boardBySize;

    public int boardSize;
    public int difficulty;

    public static GameMode mode;
    public static IntPtr handle = IntPtr.Zero;
    public static IntPtr state = IntPtr.Zero;
    public static IntPtr geometry = IntPtr.Zero;

    public Material[] playerMaterials;
    public Color[] playerColors = new Color[] { Color.white, Color.red, Color.blue };

    public event Action onPlayerTurnCommit = () => { };
    private void Awake()
    {
        loadSettings();
        initializeEngine();
    }
    void Start()
    {
        playerMaterials = new Material[playerColors.Length];
        for (int i = 0; i < playerColors.Length; ++i)
        {
            playerMaterials[i] = new Material(board.normalLineMaterial);
            playerMaterials[i].color = playerColors[i];
        }
        switch (mode)
        {
            case GameMode.TUTORIAL:
                gameObject.GetComponent<TutorialGameSession>();
                break;
            case GameMode.LOCAL_PVP:
                gameObject.AddComponent<LocalPvPGameSession>();
                break;
            case GameMode.PVAI:
                AIGameSession aIGameSession = gameObject.AddComponent<AIGameSession>();
                break;
        }
    }
    void loadSettings()
    {
        string boardSizeStr = PlayerPrefs.GetString("BoardSize");
        string difficultyStr = PlayerPrefs.GetString("Difficulty");

        OptionData difficultyData = JsonUtility.FromJson<OptionData>(difficultyStr);
        OptionData boardSizeData = JsonUtility.FromJson<OptionData>(boardSizeStr);

        boardSize = boardSizeData.values.Max();
        difficulty = difficultyData.values.Max();

        board = boardBySize[boardSize];
        board.gameObject.SetActive(true);
    }
    public bool isInProgress()
    {
        bool result = true;
        StateStatus ss = GameEngine.state_status(GameSession.state);
        if (ss != StateStatus.IN_PROGRESS)
        {
            if(mode == GameMode.PVAI)
            {
                int selfId = GetComponent<AIGameSession>().selfId;

                //--------------STATS BEGIN-----------------
                StatRecordData statRecord = SettingsManager.readStatRecord(difficulty, boardSize);
                
                if ((int)ss == selfId)
                {
                    if (selfId == 1)
                        statRecord.redWins++;
                    else
                        statRecord.blueWins++;


                    RankCalc.onWin(difficulty, boardSize);
                    GameEventMessage.SendEvent("Victory");
                }
                else
                {
                    GameEventMessage.SendEvent("Lose");
                }

                SettingsManager.writeStatRecord(difficulty, boardSize, statRecord);
                //--------------STATS END-----------------
            }
            else
            {
                GameEventMessage.SendEvent("Victory");
            }

            result = false;
        }
        return result;
    }
    void initializeEngine()
    {
        if (handle != IntPtr.Zero)
        {
            GameEngine.free_mcts_ai(handle);
            GameEngine.destroy_geometry(geometry);
            GameEngine.destroy_ai_storage(handle);
        }

        handle = GameEngine.create_ai_storage();
        geometry = GameEngine.create_std_geometry(board.width, board.length, 4, 5);

        int result = GameEngine.init_mcts_ai(handle, geometry);

        if (result != 0)
        {
            Debug.Log("Failed to initialize MCTS AI!");
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
            return;
        }
        state = GameEngine.mcts_ai_get_state(handle);
    }
    public Vector2Int getBallPos()
    {
        Vector2Int result = new Vector2Int();
        int ball = GameEngine.state_get_ball(state);
        result.x = ball % board.width;
        result.y = ball / board.width;
        return result;
    }
    public int activePlayerId()
    {
        return GameEngine.state_get_active(state);
    }
    public StateStatus getStateStatus()
    {
        return GameEngine.state_status(state);
    }
    public void undoStep()
    {
        GameEngine.mcts_ai_undo_step(handle);
    }
    public void drawTurn(Vector2Int from, Vector2Int to, Step step, int playerId)
    {
        while (!Vector2Int.Equals(from, to))
        {
            Vector2Int targetPos = PointRemap.stepToPos(from, step);

            LineRenderer lr;
            board.lines.TryGetValue(new Line(from, targetPos), out lr);

            DashedLineHighlight dashedLineHighlight = lr.gameObject.GetComponent<DashedLineHighlight>();
            if (dashedLineHighlight)
                Destroy(dashedLineHighlight);

            lr.gameObject.SetActive(true);
            lr.material = playerMaterials[playerId];
            lr.startColor = playerMaterials[playerId].color;
            lr.endColor = playerMaterials[playerId].color;

            from = targetPos;
        }
    }
}
