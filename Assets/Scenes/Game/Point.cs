﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine.Touchy;

[System.Serializable]
public enum Step
{
    NORTH_WEST = 0,
    NORTH,
    NORTH_EAST,
    EAST,
    SOUTH_EAST,
    SOUTH,
    SOUTH_WEST,
    WEST,
    QSTEPS
};
public class PointRemap
{
    private readonly static Dictionary<Swipe, Step> swipeToStepRemap = new Dictionary<Swipe, Step>
    {
        { Swipe.UpLeft,     Step.NORTH_WEST },
        { Swipe.Up,         Step.NORTH },
        { Swipe.UpRight,    Step.NORTH_EAST },
        { Swipe.Right,      Step.EAST },
        { Swipe.DownRight,  Step.SOUTH_EAST },
        { Swipe.Down,       Step.SOUTH },
        { Swipe.DownLeft,   Step.SOUTH_WEST },
        { Swipe.Left,       Step.WEST },
    };
    private readonly static Dictionary<Step, Vector2Int> stepToPosRemap = new Dictionary<Step, Vector2Int>
    {
        { Step.NORTH,       new Vector2Int(0, 1) },
        { Step.NORTH_EAST,  new Vector2Int(1, 1) },
        { Step.EAST,        new Vector2Int(1, 0) },
        { Step.SOUTH_EAST,  new Vector2Int(1, -1) },
        { Step.SOUTH,       new Vector2Int(0, -1) },
        { Step.SOUTH_WEST,  new Vector2Int(-1, -1) },
        { Step.WEST,        new Vector2Int(-1, 0) },
        { Step.NORTH_WEST,  new Vector2Int(-1, 1) }
    };
    private readonly static Dictionary<Vector2Int, Step> posToStepRemap = new Dictionary<Vector2Int, Step>
    {
        { new Vector2Int(0, 1),     Step.NORTH },
        { new Vector2Int(1, 1),     Step.NORTH_EAST },
        { new Vector2Int(1, 0),     Step.EAST },
        { new Vector2Int(1, -1),    Step.SOUTH_EAST },
        { new Vector2Int(0, -1),    Step.SOUTH },
        { new Vector2Int(-1, -1),   Step.SOUTH_WEST },
        { new Vector2Int(-1, 0),    Step.WEST },
        { new Vector2Int(-1, 1),    Step.NORTH_WEST },
    };
    private readonly static Dictionary<Step, Step> stepToRevertStep = new Dictionary<Step, Step>
    {
        { Step.NORTH,       Step.SOUTH },
        { Step.NORTH_EAST,  Step.SOUTH_WEST },
        { Step.EAST,        Step.WEST },
        { Step.SOUTH_EAST,  Step.NORTH_WEST },
        { Step.SOUTH,       Step.NORTH },
        { Step.SOUTH_WEST,  Step.NORTH_EAST },
        { Step.WEST,        Step.EAST },
        { Step.NORTH_WEST,  Step.SOUTH_EAST }
    };
    public static Step swipeToStep(Swipe swipe)
    {
        return swipeToStepRemap[swipe];
    }
    public static Vector2Int stepToPos(Vector2Int from, Step step)
    {
        return from + stepToPosRemap[step];
    }
    public static Step posToStep(Vector2Int from, Vector2Int to)
    {
        Vector2Int diff = to - from;

        int mag = Mathf.Max(Mathf.Abs(diff.x), Mathf.Abs(diff.y));
        diff.x /= mag;
        diff.y /= mag;

        return posToStepRemap[diff];
    }
    public static Step revertStep(Step step)
    {
        return stepToRevertStep[step];
    }
}

[System.Serializable]
public class Line
{
    public Material mat;
    public Color color;
    public Vector2Int p1;
    public Vector2Int p2;

    public Line(Vector2Int p1, Vector2Int p2)
    {
        this.p1 = p1;
        this.p2 = p2;
    }
    public override int GetHashCode()
    {
        return (int)(Vector2Int.Distance(p1, p2) * 1000);
    }
    public override bool Equals(object obj)
    {
        return Equals(obj as Line);
    }
    public bool Equals(Line other)
    {
        return (p1 == other.p1 && p2 == other.p2) || (p1 == other.p2 && p2 == other.p1);
    }
}
public class Point : MonoBehaviour
{
    public Board owner;
    public Vector2Int pos;
    public Dictionary<Step, Point> connections = new Dictionary<Step, Point>();
    public SpriteRenderer sprite;
    public PointType pointType;

    private void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
    }
    private void OnMouseUp()
    {
        if (this.pos == Ball.pos)
            return;
        Vector2Int deltaPos = this.pos - Ball.pos;
        if (Mathf.Abs(deltaPos.x) <= 1 && Mathf.Abs(deltaPos.y) <= 1)
        {
            LineRenderer lr;
            owner.lines.TryGetValue(new Line(Ball.pos, this.pos), out lr);

            lr.gameObject.SetActive(true);
            DashedLineHighlight highlight = lr.gameObject.AddComponent<DashedLineHighlight>();

            if (Vector3.Equals(lr.GetPosition(0), new Vector3(Ball.pos.x, Ball.pos.y, 0)))
            {
                highlight.reverse = true;
            }
            Ball.pos = this.pos;

            //GameSession.onPlayerStepTry(PointRemap.posToStep(Ball.pos, pos));
            
            //onPlayerSelection(PointRemap.posToStep(GameManager.state.ballPos, pos));
        }
        else
        {
            Debug.Log("Imposible turn!");
        }
    }
}
